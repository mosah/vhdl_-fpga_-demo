library ieee;-----------顶层文件
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity dishake is
    port(
      clk:in std_logic;-----------------------时钟
      reset_in,keym_in,keyh_in:in std_logic;-----------------------按键
		reset_out,keym_out,keyh_out:out std_logic---------输出
		--key_data_o	:	out std_logic_vector(2 downto 0)
    );
end dishake;

ARCHITECTURE ds of dishake is
    signal clk1,a1,a2,a3:std_logic;
    --signal count_3:integer;

begin
    keyr:process(clk,reset_in)
		 variable count_1:integer range 0 to 100000;
    begin
                if reset_in='0' then----若按键按下
                    if rising_edge(clk) then
                       if count_1<12 then---计数器开始计数
                        count_1:=count_1+1;
                       else
                        count_1:=count_1;
                       end if;

                       if (count_1<=11) then----若没达到一定时间长度
										a1<='1';
							  else -----若达到一定时间长度
										a1<='0';
								end if;
							end if;
                else
                count_1:=0;
					 a1<='1';
                end if;
               reset_out<=a1;-----将缓存数据赋给
    end process keyr;
	 
	 keym:process(clk,keym_in)
		 variable count_1:integer range 0 to 100000;
    begin
                if keym_in='0' then----若按键按下
                    if rising_edge(clk) then
                       if count_1<12 then---计数器开始计数
                        count_1:=count_1+1;
                       else
                        count_1:=count_1;
                       end if;

                       if (count_1<=11) then----若没达到一定时间长度
										a2<='1';
							  else -----若达到一定时间长度
										a2<='0';
								end if;
							end if;
                else
                count_1:=0;
					 a2<='1';
                end if;
               keym_out<=a2;-----将缓存数据赋给
    end process keym;
	 
	 keyh:process(clk,keyh_in)
		 variable count_1:integer range 0 to 100000;
    begin
                if keyh_in='0' then----若按键按下
                    if rising_edge(clk) then
                       if count_1<12 then---计数器开始计数
                        count_1:=count_1+1;
                       else
                        count_1:=count_1;
                       end if;

                       if (count_1<=11) then----若没达到一定时间长度
										a3<='1';
							  else -----若达到一定时间长度
										a3<='0';
								end if;
							end if;
                else
                count_1:=0;
					 a3<='1';
                end if;
               keyh_out<=a3;-----将缓存数据赋给
    end process keyh;
end ds;