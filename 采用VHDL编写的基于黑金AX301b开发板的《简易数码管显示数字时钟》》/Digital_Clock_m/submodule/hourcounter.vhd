--时计数模块
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
	
entity hourcounter is
	port(CLR,KEY1,KEY2				:in std_logic;
			R_HOURFG						:in std_logic;
			CDatahl		:in std_logic_vector(3 downto 0);
			CDatahh		:in std_logic_vector(3 downto 0);
			DATA4,DATA5					:out std_logic_vector(3 downto 0));
end hourcounter;
 
architecture cnt of hourcounter is
	signal cnt0:std_logic_vector(3 downto 0):="0000";
	signal cnt1:std_logic_vector(3 downto 0):="0000";
begin
	process(CLR,KEY1,KEY2)
	begin
	if(CLR='1' and KEY1='1' and KEY2='1')	then
		if rising_edge(R_HOURFG)		then
			cnt0<=cnt0+1;
			if(cnt0=9)	then
				cnt0<="0000";
				cnt1<=cnt1+1;
			end if;
			if(cnt1="0010" and cnt0="0011")		then  --23
				cnt0<="0000";
				cnt1<="0000";
			end if;
		end if;
	elsif(CLR='0' and KEY1='1' and KEY2='1')		then
		cnt0<="0000";
		cnt1<="0000";
	elsif(CLR='1' and KEY1='0')	then
		cnt0<=CDatahl;
		cnt1<=CDatahh;
	else null;
	end if;	
		DATA4<=cnt0;
		DATA5<=cnt1;
	end process;
end cnt;