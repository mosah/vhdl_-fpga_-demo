-- 调时置数
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
	
entity setnum is
	port(	CLK,CLR,KEY1,KEY2:in std_logic;
				--Key_data					:in std_logic_vector(2 downto 0);
				mdatal,mdatah,hdatal,hdatah:out std_logic_vector(3 downto 0));
end setnum;
 
architecture drt of setnum is
	signal cnt_ml		:std_logic_vector(3 downto 0):="0000";	--min	low
	signal cnt_mh	:std_logic_vector(3 downto 0):="0000";	--min	high
	signal cnt_hl		:std_logic_vector(3 downto 0):="0000";	--hour	low
	signal cnt_hh		:std_logic_vector(3 downto 0):="0000";	--hour	high
	--	-----------------
begin
		process(CLK,CLR,KEY1,KEY2)--change
		begin
		if(CLR='1')		then
			if(KEY1='1' and KEY2='0')		then		--'按键2'按下,调分
				if(CLK'event and CLK='1')	then
					cnt_ml<=cnt_ml+1;
					if(cnt_ml=9)then
						cnt_ml<="0000";
						cnt_mh<=cnt_mh+1;
					end if;
					if(cnt_mh=5 and cnt_ml=9)		then
						cnt_ml<="0000";
						cnt_mh<="0000";
					end if;
				end if;
			mdatal<=cnt_ml;
			mdatah<=cnt_mh;
			elsif(KEY1='0' and KEY2='1')		then		--'按键1',调时
				if(CLK'event and CLK='1')		then
					cnt_hl<=cnt_hl+1;
					if(cnt_hl=9)then
						cnt_hl<="0000";
						cnt_hh<=cnt_hh+1;
					end if;
					if(cnt_hh=2 and cnt_hl=3)then
						cnt_hl<="0000";
						cnt_hh<="0000";
					end if;
				end if;
			hdatal<=cnt_hl;
			hdatah<=cnt_hh;
			else null;
			end if;
		else
		cnt_ml<="0000";		cnt_mh<="0000";		cnt_hl<="0000";		cnt_hh<="0000";
		mdatal<="0000";		mdatah<="0000";		hdatal<="0000";		hdatah<="0000";
		end if;
		end process;
end drt;