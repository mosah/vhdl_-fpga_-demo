library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity minutecounter is
	port(	CLR				:in std_logic;		--重启信号
				R_MINFG	:in std_logic;
				KEY1,KEY2	:in std_logic;
				CDataml		:in std_logic_vector(3 downto 0);
				CDatamh	:in std_logic_vector(3 downto 0);
				HOURFG		:out std_logic;	--小时进位信号
				DATA2		:out std_logic_vector(3 downto 0);
				DATA3		:out std_logic_vector(3 downto 0));
end minutecounter;
 
architecture cnt of minutecounter is
	signal cnt0:std_logic_vector(3 downto 0):="0000";--
	signal cnt1:std_logic_vector(3 downto 0):="0000";--
begin
	process(CLR,KEY1,KEY2)
	begin
	if(CLR='1' and KEY1='1'	and	KEY2='1')		then	--无按键按下
		if rising_edge(R_MINFG)	then
			cnt0<=cnt0+1;
			if(cnt0=9)	then
				cnt0<="0000";
				cnt1<=cnt1+1;
			end if;
			if(cnt1="0101" and cnt0="1001")		then		--分钟满位,进位信号置'1'
				cnt0<="0000";
				cnt1<="0000";
				HOURFG<='1';
			else
				HOURFG<='0';
			end if;
		end if;
	elsif(CLR='0' and KEY1='1' and KEY2='1')	then	--重置键按下
		cnt0<="0000";
		cnt1<="0000";
		HOURFG<='0';
	elsif(CLR='1' and KEY2='0')	then
		cnt0<=CDataml;
		cnt1<=CDatamh;
	else null;
	end if;
	DATA2<=cnt0;
	DATA3<=cnt1;
	end process;
end cnt;