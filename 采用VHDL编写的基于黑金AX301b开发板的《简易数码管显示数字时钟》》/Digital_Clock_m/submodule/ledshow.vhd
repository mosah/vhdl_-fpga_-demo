--数码管译码与显示
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
 
entity ledshow is
		port(	CLK					:in std_logic;
					S_SD_L			:in std_logic_vector(3 downto 0);
					S_SD_H			:in std_logic_vector(3 downto 0);
					M_SD_L			:in std_logic_vector(3 downto 0);
					M_SD_H			:in std_logic_vector(3 downto 0);
					H_SD_L			:in std_logic_vector(3 downto 0);
					H_SD_H			:in std_logic_vector(3 downto 0);

					seg		:out std_logic_vector(7 downto 0);	--段选
					sel		:out std_logic_vector(5 downto 0));	--位选
end	ledshow;
 
architecture sw of ledshow is
	signal cnt7:integer range 0 to 6;	
	signal	data: std_logic_vector(3 downto 0);
begin
	process(CLK)
		begin
			if CLK'event and CLK='1' then
				cnt7<=cnt7+1;
			end if;
	end process;
	
	process(cnt7)
		begin
				case cnt7 is
					when 0 =>sel<="011111";data<=S_SD_L(3 downto 0);
					when 1 =>sel<="101111";data<=S_SD_H(3 downto 0);
					when 2 =>sel<="110111";data<=M_SD_L(3 downto 0);
					when 3 =>sel<="111011";data<=M_SD_H(3 downto 0);
					when 4 =>sel<="111101";data<=H_SD_L(3 downto 0);
					when 5 =>sel<="111110";data<=H_SD_H(3 downto 0);
					when others=>data<="1111";
				end case;
	end process;
 
	process(data)
		begin
			case data(3 downto 0) is
				--共阳极数码管，当某一字段对应的引脚为低电平时，相应字段就点亮
				--当控制引脚为低电平时，对应的数码管有了供电电压
				--------        译码表		  ---------
				when "0000"=>seg<="11000000";		--'0'
				when "0001"=>seg<="11111001";		--'1'
				when "0010"=>seg<="10100100";		--'2'
				when "0011"=>seg<="10110000";		--'3'
				when "0100"=>seg<="10011001";		--'4'
				when "0101"=>seg<="10010010";		--'5'
				when "0110"=>seg<="10000010";		--'6'
				when "0111"=>seg<="11111000";		--'7'
				when "1000"=>seg<="10000000";		--'8'
				when "1001"=>seg<="10010000";		--'9'
				when others=>seg<="11111111";
			end case;
	end process;
end	sw;