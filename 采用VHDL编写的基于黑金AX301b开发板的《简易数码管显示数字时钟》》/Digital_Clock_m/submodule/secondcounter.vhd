-- 秒钟计时模块
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity secondcounter is	--端口定义
	port(	CLK			:in std_logic;
				CLR			:in std_logic; --重启信号
				KEY1		:in std_logic;	--调节信号--高电平按键未按下
				KEY2		:in std_logic;
				MINFG	:out std_logic;				--分钟进位标志
				DATA0	:out std_logic_vector(3 downto 0);--数据输出
				DATA1	:out std_logic_vector(3 downto 0));
end secondcounter;

architecture cnt of secondcounter is
	signal cnt0:std_logic_vector(3 downto 0):="0000";
	signal cnt1:std_logic_vector(3 downto 0):="0000";
 
begin
	process(CLK,CLR,KEY1,KEY2)
	begin
	if(CLR='1' and KEY1='1'	and	KEY2='1')			then	
		if(CLK'event and CLK='1')	then
			cnt0<=cnt0+1;
			if(cnt0="1001")	then	--9
				cnt0<="0000";
				cnt1<=cnt1+1;	--进一位
			end if;
			if(cnt1="0101" and cnt0="1001")		then	--秒钟满值,清零
				cnt0<="0000";
				cnt1<="0000";
				MINFG<='1';
			else
				MINFG<='0';
			end if;
		end if;
	elsif(CLR='0' or KEY1='0' or KEY2='0')	then	--当有任意按键按下时秒钟皆置零
		cnt0<="0000";
		cnt1<="0000";
	else null;
	end if;

	end process;
	DATA0<=cnt0;
	DATA1<=cnt1;
end cnt;