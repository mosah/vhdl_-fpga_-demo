--时钟源模块，提供1Hz的时钟信号；500Hz、1000Hz的蜂鸣器频响
--	对外部晶振提供的50MHz信号分频
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Clk_Sourse is
	port(	CLK		:in std_logic;   --开发板提供了50MHz的外部晶振
				CLK_1Hz,CLK_5Hz,CLK_500Hz,CLK_1000Hz	:out std_logic);
end Clk_Sourse;
 
architecture rtl of Clk_Sourse is
	--	clk flag
	signal CFG_1,CFG_5,CFG_500,CFG_1000:std_logic;
begin
	process(CLK)
	variable cnt_1:integer range 0 to 24999999;
	variable cnt_5:integer range 0 to 4999999;
	variable cnt_500:integer range 0 to 49999;
	variable cnt_1000:integer range 0 to 24999;
	begin
		if(CLK'event and CLK='1') then
			cnt_1:=cnt_1+1;
			cnt_5:=cnt_5+1;
			cnt_500:=cnt_500+1;
			cnt_1000:=cnt_1000+1;
			if(cnt_1=24999999) then
				CFG_1<=not CFG_1;	--取反
				cnt_1:=0;	--置零
			end if;
			if(cnt_5=4999999) then
				CFG_5<=not CFG_5;	--取反
				cnt_5:=0;	--置零
			end if;
			
			if(cnt_500=49999) then
				CFG_500<=not CFG_500;	--取反
				cnt_500:=0;	--置零
			end if;
			if(cnt_1000=24999) then
				CFG_1000<=not CFG_1000;	--取反
				cnt_1000:=0;	--置零
			end if;
		end if;
	end process;
	CLK_1Hz<=CFG_1;
	CLK_5Hz<=CFG_5;
	CLK_500Hz<=CFG_500;
	CLK_1000Hz<=CFG_1000;
end rtl;