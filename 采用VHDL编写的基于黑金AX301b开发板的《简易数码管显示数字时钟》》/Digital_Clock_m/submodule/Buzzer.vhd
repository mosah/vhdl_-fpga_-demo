--beep
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity Buzzer is
	port(	CLK,CLK_500Hz,CLK_1000Hz	:	in	std_logic;
				CLR,KEY1,KEY2		:	in std_logic;
				bdata2,bdata3,bdata0,bdata1	:in	std_logic_vector(3	downto	0);
				Beex,BLed	:	out std_logic);
	end Buzzer;
	
architecture bp of Buzzer is
		signal data_m		:std_logic_vector(7 downto 0);
		signal data_s			:std_logic_vector(7 downto 0);
		
begin
		process(CLR)
		begin
		data_m<=bdata3 & bdata2;
		data_s<=bdata1 & bdata0;
		if(CLR='1' and KEY1='1' and KEY2='1') then 
			if(data_m="00000000")	then
				if(data_s="00000000") then
					Beex<=CLK_1000Hz;
					BLed<=CLK_1000Hz;
				else
					Beex<='1';
					BLed<='0';
				end if;
			elsif(data_m="01011001") then
				case data_s is
					when "01010000" | "01010010" | "01010100" | "01010110" | "01011000"=>Beex<=CLK_500Hz;BLed<=CLK;
					when others=>Beex<='1';BLed<='0';
				end case;
			end if;
		end if;
		end process;
end bp;
--5&9——0101 1001
--5&0——0101 0000
--5&2——0101 0010
--5&4——0101 0100
--5&6——0101 0110
--5&8——0101 1000
	