----顶层文件
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity Digital_Clock	is
	port(	CLK,Key_reset_u,Set_min_u,Set_hour_u		:in 	std_logic;
				Beep,BLed:out	 std_logic;
				Led_Sel:out	std_logic_vector(5	downto	0);
				Led_Seg:out	std_logic_vector(7	downto	0));
end Digital_Clock;

architecture fl of Digital_Clock is
	component		Clk_Sourse
		port(	CLK		:in std_logic;   --开发板提供了50MHz的外部晶振
					CLK_1Hz,CLK_5Hz,CLK_500Hz,CLK_1000Hz	:out std_logic);
	end	component;
	
	component		secondcounter
		port(	CLK			:in std_logic;
					CLR			:in std_logic; --重启信号
					KEY1		:in std_logic;	--调节信号--高电平按键未按下
					KEY2		:in std_logic;
					MINFG	:out std_logic;				--分钟进位标志
					DATA0	:out std_logic_vector(3 downto 0);--数据输出
					DATA1	:out std_logic_vector(3 downto 0));
	end	component;
	
	component		minutecounter
	port(	
				CLR				:in std_logic; --重启信号
				R_MINFG	:in std_logic;
				KEY1			:in std_logic;	--调节信号--高电平按键未按下
				KEY2			:in std_logic;
				CDataml		:in std_logic_vector(3 downto 0);
				CDatamh	:in std_logic_vector(3 downto 0);
				HOURFG		:out std_logic;				--分钟进位标志
				DATA2		:out std_logic_vector(3 downto 0);--数据输出
				DATA3		:out std_logic_vector(3 downto 0));
	end	component;
	
	component	 hourcounter 
	port(CLR,KEY1,KEY2				:in std_logic;
			R_HOURFG						:in std_logic;
			CDatahl		:in std_logic_vector(3 downto 0);
			CDatahh		:in std_logic_vector(3 downto 0);
			DATA4,DATA5					:out std_logic_vector(3 downto 0));
	end component	;
	
	component		setnum
	port(	CLK,CLR,KEY1,KEY2:in std_logic;
				mdatal,mdatah,hdatal,hdatah:out std_logic_vector(3 downto 0));
	end component;
	
	component  ledshow
		port(	CLK					:in std_logic;
					S_SD_L			:in std_logic_vector(3 downto 0);
					S_SD_H			:in std_logic_vector(3 downto 0);
					M_SD_L			:in std_logic_vector(3 downto 0);
					M_SD_H			:in std_logic_vector(3 downto 0);
					H_SD_L			:in std_logic_vector(3 downto 0);
					H_SD_H			:in std_logic_vector(3 downto 0);

					--data	:buffer std_logic_vector(3 downto 0);
					seg		:out std_logic_vector(7 downto 0);	--段选
					sel		:out std_logic_vector(5 downto 0));	--位选
	end component;
	
	component Buzzer
		port(	CLK,CLK_500Hz,CLK_1000Hz	:	in	std_logic;
					CLR,KEY1,KEY2		:	in std_logic;
					bdata2,bdata3,bdata0,bdata1	:in	std_logic_vector(3	downto	0);
					Beex,BLed	:	out std_logic);
	end component	;
	
	component dishake
		port(	clk,reset_in,keym_in,keyh_in:in std_logic; --按键按下时为'0'
					reset_out,keym_out,keyh_out:out std_logic);
	end component	;
	
--------------------------------------------------------------------------------------------------
	signal	CLK_1Hz,CLK_5Hz,CLK_500Hz,CLK_1000Hz,Beex	:	std_logic;
	signal MINFG	,HOURFG	:	std_logic;
	signal	Co_h,Co_m	:	std_logic;
	signal D_net0,D_net1,D_net2,D_net3,D_net4,D_net5	:	std_logic_vector(3 downto 0);
	signal cmdatal,cmdatah,chdatal,chdatah	:	std_logic_vector(3	downto	0);
	signal BEEPS,BEEPM	: std_logic;
	signal	Key_reset,Set_hour,Set_min	:	std_logic;
	
begin
	u1:	Clk_Sourse	port map(CLK,CLK_1Hz,CLK_5Hz,CLK_500Hz,CLK_1000Hz);
	u2:	secondcounter	port map(CLK=>CLK_1Hz,CLR=>Key_reset,KEY1=>Set_hour,KEY2=>Set_min,
															MINFG=>Co_m,DATA1=>D_net1,DATA0=>D_net0);
	------------------------------------------------------------------------------------------------------------------------------------------												
	u3:	minutecounter	port map(CLR=>Key_reset,KEY1=>Set_hour,KEY2=>Set_min,
															HOURFG=>Co_h,R_MINFG=>Co_m,DATA3=>D_net3,DATA2=>D_net2,
															CDataml=>cmdatal,CDatamh=>cmdatah);
	------------------------------------------------------------------------------------------------------------------------------------------
	u4:	hourcounter	port map(CLR=>Key_reset,KEY1=>Set_hour,KEY2=>Set_min,
															R_HOURFG=>Co_h,DATA5=>D_net5,DATA4=>D_net4,
															CDatahl=>chdatal,CDatahh=>chdatah);
	------------------------------------------------------------------------------------------------------------------------------------------								
	u5:	setnum	port map(CLK=>CLK_5Hz,CLR=>Key_reset,KEY1=>Set_hour,KEY2=>Set_min,
												mdatal=>cmdatal,mdatah=>cmdatah,hdatal=>chdatal,hdatah=>chdatah);
	u6:  Buzzer port map(CLK=>CLK_5Hz,CLK_500Hz=>CLK_500Hz,CLK_1000Hz=>CLK_1000Hz,Beex=>Beep,BLed=>BLed,
											bdata2=>D_net2,bdata3=>D_net3,bdata0=>D_net0,bdata1=>D_net1,
											CLR=>Key_reset,KEY1=>Set_hour,KEY2=>Set_min);
		------------------------------------------------------------------------------------------------------------------------------------------			
	u7:	ledshow port map(	CLK=>CLK_1000Hz,
												S_SD_L=>D_net0,	S_SD_H=>D_net1,
												M_SD_L=>D_net2,	M_SD_H=>D_net3,
												H_SD_L=>D_net4,	H_SD_H=>D_net5,
												seg=>Led_Seg,sel=>Led_Sel);
	u8:	dishake port map(clk=>CLK_1000Hz,reset_in=>Key_reset_u,keym_in=>Set_min_u,keyh_in=>Set_hour_u,
											reset_out=>Key_reset,keym_out=>Set_min,keyh_out=>Set_hour);
end	fl;